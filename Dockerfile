#####
## Dockerfile for the Soter API
#####

FROM python:3.7-alpine AS build

# Install git for setuptools_scm
RUN apk add --no-cache git

# Build wheels for the dependencies
COPY requirements.txt /application/
RUN pip wheel \
      --wheel-dir /wheelhouse \
      --no-deps \
      --requirement /application/requirements.txt
# Then build a wheel for the app itself
COPY . /application
RUN pip wheel \
      --wheel-dir /wheelhouse \
      --no-deps \
      /application


FROM alpine as trivy

RUN apk add --no-cache curl

# Download and unpack trivy
ARG TRIVY_VERSION=0.12.0
WORKDIR /opt/trivy
RUN curl -fsSL -o trivy.tar.gz https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz && \
    tar -xzf trivy.tar.gz


FROM sotersec/soter-asgi

# Install the rpm command, which is required to scan CentOS/RHEL-based images
RUN apk --no-cache add ca-certificates rpm

# Make the Trivy cache directory and set it's ownership
ENV TRIVY_CACHE_DIR /var/cache/trivy
RUN mkdir -p ${TRIVY_CACHE_DIR} && \
    chown $ASGI_UID:$ASGI_GID ${TRIVY_CACHE_DIR}

# Install trivy
COPY --from=trivy /opt/trivy/trivy /usr/local/bin/
COPY --from=trivy /opt/trivy/contrib/*.tpl /contrib/

# Install the wheels built in the previous stage
COPY --from=build /wheelhouse /wheelhouse
RUN pip install --no-deps /wheelhouse/*.whl

# Ensure that the API runs as the ASGI user
USER $ASGI_UID
# Run the Soter API app
CMD ["/asgi-run.sh", "soter.trivy:app"]
