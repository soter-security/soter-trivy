"""
Module defining the configuration options for soter-trivy.
"""

import os

from pydantic import PositiveInt

from ..scanner.config import ConfigModel


class Settings(ConfigModel):
    """
    Settings for the Soter Trivy scanner.
    """
    class Config:
        default_config_path = "/etc/soter/trivy.toml"
        env_prefix = "SOTER_TRIVY_"

    command: str = 'trivy'
    concurrent_scans: PositiveInt = 1
    db_update_interval: PositiveInt = 86400


# Load the settings from the specified config file
settings = Settings(_path = os.environ.get('SOTER_TRIVY_CONFIG_FILE'))
